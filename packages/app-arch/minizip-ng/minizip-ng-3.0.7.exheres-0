# Copyright 2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=zlib-ng ] cmake [ ninja=true ]

SUMMARY="Fork of the popular zip manipulation library found in the zlib distribution"

LICENCES="ZLIB"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    zstd
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# 27 tests failed out of 239, last checked: 3.0.7
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-arch/bzip2
        app-arch/xz
        sys-libs/zlib
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        zstd? ( app-arch/zstd )
    test:
        dev-cpp/gtest
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/be23c8d3b7e2cb5ba619e60517cad277ee510fb7.patch
    "${FILES}"/d7a10bb868b1e9e27078e8ec64d6c8dd80a1ccd0.patch
    "${FILES}"/4d13283bad56fbf34e9ea80074e5c5759cf70df6.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_ZLIBNG:BOOL=TRUE
    -DMZ_BUILD_FUZZ_TESTS:BOOL=FALSE
    -DMZ_BZIP2:BOOL=TRUE
    -DMZ_CODE_COVERAGE:BOOL=FALSE
    -DMZ_COMPAT:BOOL=FALSE
    -DMZ_COMPRESS_ONLY:BOOL=FALSE
    -DMZ_DECOMPRESS_ONLY:BOOL=FALSE
    -DMZ_FETCH_LIBS:BOOL=FALSE
    -DMZ_FORCE_FETCH_LIBS:BOOL=FALSE
    -DMZ_ICONV:BOOL=TRUE
    -DMZ_LIBBSD:BOOL=FALSE
    -DMZ_LIBCOMP:BOOL=FALSE
    -DMZ_LZMA:BOOL=TRUE
    -DMZ_OPENSSL:BOOL=TRUE
    -DMZ_PKCRYPT:BOOL=TRUE
    -DMZ_SIGNING:BOOL=TRUE
    -DMZ_WZAES:BOOL=TRUE
    -DMZ_ZLIB:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'zstd MZ_ZSTD'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DMZ_BUILD_TESTS:BOOL=TRUE -DMZ_BUILD_TESTS:BOOL=FALSE'
    '-DMZ_BUILD_UNIT_TESTS:BOOL=TRUE -DMZ_BUILD_UNIT_TESTS:BOOL=FALSE'
)

