# Copyright 2009-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

export_exlib_phases src_install

SUMMARY="A Linux kernel packet filter control tool"
DESCRIPTION="
iptables is built on top of netfilter, the packet alteration framework for Linux.
It is used to control packet filtering, Network Address Translation (masquerading,
portforwarding, transparent proxying), and special effects such as packet mangling.
"
HOMEPAGE="https://netfilter.org/projects/${PN}"
DOWNLOADS="${HOMEPAGE}/files/${PNV}.tar.xz"

UPSTREAM_CHANGELOG="${HOMEPAGE}/files/changes-iptables-${PV}.txt [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="https://netfilter.org/documentation"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

# previously tests were skipped, now they fail, last checked: 2.8.9
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-kernel/linux-headers[>=4.4]
        virtual/pkg-config
    build+run:
        dev-libs/libpcap
        net-libs/libmnl[>=1.0]
        net-libs/libnetfilter_conntrack[>=1.0.6]
        net-libs/libnfnetlink[>=1.0]
        net-libs/libnftnl[>=1.2.6]
    test:
        dev-lang/python:*[>=3]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --without-kernel
    --enable-bpf-compiler
    --enable-connlabel
    --enable-devel
    --enable-libipq
    --enable-libnfnetlink
    --enable-nfsynproxy
    --enable-nftables
    --disable-profiling
    --disable-static

    # NOTE(somasis) declared by default by glibc, but needed
    #               on musl to expose constants iptables needs
    CFLAGS="${CFLAGS} -D_GNU_SOURCE"
)

DEFAULT_SRC_COMPILE_PARAMS=( V=1 )

DEFAULT_SRC_INSTALL_EXCLUDE=( "release.sh" )

iptables_src_install() {
    default

    keepdir /etc/xtables
    keepdir /var/lib/iptables
    keepdir /var/lib/ip6tables

    install_systemd_files
}

