# Copyright 2011 Michael Thomas <aelmalinka@gmail.com>
# Copyright 2017 Arnaud Lefebvre <a.lefebvre@outlook.fr>
# Distributed under the terms of the GNU General Public License v2

MY_PV="v${PV}"
MY_PNV="${PN}-${MY_PV}"
WORK="${WORKBASE}/${MY_PNV}"

require python [ blacklist=2 multibuild=false ]
export_exlib_phases src_configure src_install src_test

DESCRIPTION="
Designed to provide an easy way to build scalable network applications, similar to Event Machine or
Twisted.
"
HOMEPAGE="https://nodejs.org"
DOWNLOADS="${HOMEPAGE}/dist/${MY_PV}/${MY_PNV}.tar.xz"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    gdb [[ description = [ Enable GDB JIT interface for v8 ] ]]
    bundled-openssl [[
        description = [ Build node against the bundled OpenSSL instead of the system installed one ]
    ]]
"
# TODO(japaric) re-add libressl provider when libressl catches up with the new API in openssl 1.0.2
# OR when node officially supports libressl cf. https://github.com/nodejs/node/issues/428
# context: libressl is on API parity with openssl 1.0.1, but node uses the new API available in
# openssl 1.0.2 cf. https://github.com/nodejs/node/issues/2783
#   ( providers: libressl openssl  ) [[ number-selected = exactly-one  ]]

RESTRICT="test"

# See TODO above
#        providers:libressl? ( dev-libs/libressl:= )
#        providers:openssl? ( dev-libs/openssl:=  )
# >= 16.0.0: shared ngtcp2
# >= 20.18.0: shared uvwasi
# >= 22.12.0: bundles currently still masked dev-libs/icu:=[>=76.1]
DEPENDENCIES="
    build+run:
        app-arch/brotli[>=1.1.0]
        dev-libs/icu:=[>=75.1]
        dev-libs/libuv[>=1.44.2]
        net-dns/c-ares[>=1.29.0]
        net-libs/nghttp2[>=1.61.0]
        sys-libs/zlib[>=1.3.0]
        !bundled-openssl? ( dev-libs/openssl:=[>=3.0.15] )
"

if ever at_least 20.18.0; then
    DEPENDENCIES+="
        build+run:
            dev-libs/libuv[>=1.48.0]
            net-dns/c-ares[>=1.34.4]
    "
fi

if ever at_least 22.13.0; then
    DEPENDENCIES+="
        build+run:
            dev-db/sqlite:3[>=3.47.2]
            dev-libs/libuv[>=1.49.2]
            net-libs/nghttp2[>=1.64.0]
    "
fi

DEFAULT_SRC_COMPILE_PARAMS=( V=1 LD="${CXX}" )

PATH="${TMPDIR}:${PATH}"

node_src_configure() {
    local myconf=(
        --prefix=/usr/$(exhost --target)
        --openssl-use-def-ca-store
        --shared-brotli
        --shared-cares
        --shared-libuv
        --shared-nghttp2
        --shared-zlib
        --with-intl=system-icu
        $(option gdb && echo --gdb)
        $(option !bundled-openssl && echo --shared-openssl)
    )

    if ever at_least 22.11.0; then
        myconf+=( --shared-sqlite )
    fi

    edo ${PYTHON} ./configure.py "${myconf[@]}"
}

node_src_install() {
    default

    # man files for node
    doman "${IMAGE}"/usr/$(exhost --target)/share/man/man1/*
    # man files for npm cli
    doman "${IMAGE}"/usr/$(exhost --target)/lib/node_modules/npm/man/man{1,5,7}/*.[157]

    edo mkdir -p "${IMAGE}"/usr/share/doc/"${PNVR}"/
    edo mv "${IMAGE}"/usr/$(exhost --target)/share/doc/${PN}/* "${IMAGE}"/usr/share/doc/"${PNVR}"/
    edo rm -r "${IMAGE}"/usr/$(exhost --target)/{share/{doc/{${PN}/,},},lib/node_modules/npm/man}
}

node_src_test() {
    # rebuild certificates
    emake -j1 -C test/fixtures/keys clean
    emake -j1 -C test/fixtures/keys

    esandbox allow_net "unix:${WORK}/test/tmp/test.sock"

    emake test
}

