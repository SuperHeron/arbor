# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# multiple patches
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

export_exlib_phases src_prepare

SUMMARY="A programming language designed for the analysis of computer languages"
DESCRIPTION="
Colm is influenced primarily by TXL. It is in the family of program
transformation languages.
A transformation language has a type system based on formal languages. Rather
than define classes or data structures, one defines grammars. A parser is
constructed automatically from the grammar, and the parser is used for two
purposes: to parse the input language, and to parse the structural patterns in
the program that performs the analysis.

Colm’s main contribution lies in the parsing method. Colm’s parsing engine is
generalized, but it also allows for the construction of arbitrary global data
structures that can be queried during parsing. In other generalized methods,
construction of global data requires some very careful consideration because
of inherent concurrency in the parsing method. It is such a tricky task that
it is often avoided altogether and the problem is deferred to a post-parse
disambiguation of the parse forest."

HOMEPAGE="https://www.colm.net/open-source/${PN}/"
DOWNLOADS="https://www.colm.net/files/${PN}/${PNV}.tar.gz"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        doc? (
            app-doc/asciidoc
            dev-python/Pygments
        )
"

DEFAULT_SRC_CONFIGURE_PARAMS+=(
    # Overwrite the default --datadir=/usr/share
    --datadir=/usr/share/${PN}
    --disable-static
    --enable-program
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES+=(
    'doc manual'
)

colm_src_prepare() {
    autotools_src_prepare

    edo sed \
        -e "s/echo gcc/echo $(exhost --tool-prefix)cc/" \
        -e "s/echo g++/echo $(exhost --tool-prefix)c++/" \
        -i test/colm.d/gentests.sh
}

