# Copyright 2008 Alexander Færøy <eroyf@exherbo.org>
# Copyright 2011 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service udev-rules
require openrc-service [ openrc_confd_files=[ "${FILES}"/openrc/confd ] ]

SUMMARY="ALSA utilities"
DESCRIPTION="
The following utilities are included:
* aconnect - ALSA sequencer connection manager
* alsactl - advanced controls and daemon (store/restore/init/remove hardware settings)
* alsaloop - create a PCM loopback between a PCM capture device and a PCM playback device
* alsamixer - ncurses-based mixer
* alsaucm - ALSA Use Case Manager
* amidi - read from and write to ALSA RawMIDI ports
* amixer - command-line mixer
* aplay - command-line sound player
* aplaymidi - play standard MIDI files
* aplaymidi2 - play MIDI 2.0 Clip files
* arecord - command-line sound recorder
* arecordmidi - record standard MIDI files
* arecordmidi2 - record MIDI 2.0 Clip files
* aseqdump - show the events received at an ALSA sequencer port
* aseqnet - ALSA sequencer connectors over network
* aseqsend - send arbitrary messages to selected ALSA MIDI seqencer port
* axfer - command-line recorder and player
* iecset - Set or dump IEC958 status bits
* speaker-test - command-line speaker test tone generator
"
HOMEPAGE="https://www.alsa-project.org"
DOWNLOADS="mirror://alsaproject/${PN#alsa-}/${PNV}.tar.bz2"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    alsaloop [[ description = [ Tool to create playback/capture loops ] ]]
    bat [[ description = [ Tool to test audio card output ] ]]
    ( linguas: de eu fr ja ka ko sk )
"

# NOTE: automagic dependency on libffado (not recommended/technical preview)
DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config
    build+run:
        sys-libs/ncurses
        sys-sound/alsa-lib[>=1.2.13]
        alsaloop? ( media-libs/libsamplerate [[ note = [ Used by alsaloop, which is optional ] ]] )
        bat? ( sci-libs/fftw )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/f90124c73edd050b24961197a4abcf17e53b41a8.patch
    "${FILES}"/6f7ce73159c02b726a6f0fe0acff961c95e337a8.patch
    "${FILES}"/d45a2be9eaba2bb75840ca7c19bccc4d6578270b.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-alsatopology
    --enable-nhlt
    --enable-nls
    --disable-alsabat-backend-tiny
    # disable alsaconf like all other distros do
    --disable-alsaconf
    --disable-rst2man
    --disable-static
    --disable-xmlto
    --with-alsactl-lock-dir=/run/lock
    --with-plugindir=/usr/$(exhost --target)/lib/alsa-topology
    --with-systemdsystemunitdir="${SYSTEMDSYSTEMUNITDIR}"
    --with-udev-rules-dir="${UDEVRULESDIR}"
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( alsaloop bat )

src_install() {
    default

    keepdir /var/lib/alsa

    dodir /etc/alsa
    edo echo "# Remove this file to disable the alsactl daemon mode" > \
        "${IMAGE}"/etc/alsa/state-daemon.conf

    edo rmdir "${IMAGE}"/usr/share/man/man7

    install_openrc_files
}

