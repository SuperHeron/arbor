# Copyright 2010 Jan Meier <jan@codejunky.org>
# Copyright 2008-2017 Wulf C. Krueger
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'ppp-2.4.4-r21.ebuild' from Gentoo, which is:
#       Copyright 1999-2008 Gentoo Foundation

require pam

SCRIPT_VER="20080922"

SUMMARY="An implementation of the Point-to-Point Protocol (PPP)"
DESCRIPTION="
ppp (Paul's PPP Package) is an open source package which implements the
Point-to-Point Protocol (PPP) on Linux and Solaris systems.
"
HOMEPAGE="https://ppp.samba.org"
DOWNLOADS="
    https://download.samba.org/pub/${PN}/${PNV}.tar.gz
    https://dev.exherbo.org/~philantrop/distfiles/${PN}-2.4.4-scripts-${SCRIPT_VER}.tar.bz2
"

UPSTREAM_CHANGELOG="https://ppp.samba.org/README.html"
UPSTREAM_DOCUMENTATION="https://ppp.samba.org/documentation.html"

LICENCES="BSD-3 GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    activefilter [[ description = [ Enables activefilter support ] ]]
    atm          [[ description = [ Enables support for PPP over ATM (PPPoA) ] ]]
    pam
    radius
    systemd

    ( libc: musl )
    ( providers: libressl openssl )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        activefilter? ( dev-libs/libpcap[>=0.9.8] )
        atm? ( net-dialup/linux-atm )
        !libc:musl? ( dev-libs/libxcrypt:= )
        pam? ( sys-libs/pam )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-cbcp
    --enable-eaptls
    --enable-ipv6cp
    --enable-microsoft-extensions
    --enable-openssl-engine
    --enable-peap
    --enable-plugins
    --disable-mslanman
    --disable-multilink
    --with-openssl
    --without-gtk
    --without-srp
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    systemd
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'activefilter pcap'
    atm
    pam
)

src_prepare() {
    default

    # set the right paths in radiusclient.conf
    edo sed \
        -e "s:/usr/local/etc:/etc:" \
        -e "s:/usr/local/sbin:/usr/$(exhost --target)/bin:" \
        -i pppd/plugins/radius/etc/radiusclient.conf

    # set config dir to /etc/ppp/radius
    edo sed \
        -e "s:/etc/radiusclient:/etc/ppp/radius:g" \
        -i pppd/plugins/radius/{*.8,*.c,*.h,etc/*}
}

src_install() {
    local i
    for i in chat pppd pppdump pppstats
    do
        doman ${i}/${i}.8
        dobin ${i}/${i}
    done
    edo chmod u+s-w "${IMAGE}"/usr/$(exhost --target)/bin/pppd

    # Install pppd header files
    insinto /usr/$(exhost --target)/include/pppd
    doins pppd/*.h

    # Install pkg-config file
    insinto /usr/$(exhost --target)/lib/pkgconfig
    doins pppd/pppd.pc

    dobin pppd/plugins/pppoe/pppoe-discovery

    keepdir /etc/ppp/peers
    insinto /etc/ppp
    insopts -m0600
    newins etc.ppp/pap-secrets pap-secrets.example
    newins etc.ppp/chap-secrets chap-secrets.example

    insopts -m0644
    doins etc.ppp/{eaptls-client,eaptls-server,openssl.cnf,options}

    exeinto /etc/ppp
    for i in ip-up ip-down ; do
        doexe "${WORKBASE}"/scripts/${i}
        insinto /etc/ppp/${i}.d
        dosym ${i} /etc/ppp/${i/ip/ipv6}
        doins "${WORKBASE}"/scripts/${i}.d/*
    done

    pamd_mimic_system ppp auth auth account session

    insinto /usr/$(exhost --target)/lib/pppd/${PV}
    insopts -m0755
    doins pppd/plugins/.libs/{minconn,passprompt,passwordfd,winbind}.so
    doins pppd/plugins/pppoe/.libs/pppoe.so
    doins pppd/plugins/pppol2tp/.libs/{openl2tp,pppol2tp}.so

    if option atm; then
        doins pppd/plugins/pppoatm/.libs/pppoatm.so
    fi

    if option radius; then
        doins pppd/plugins/radius/.libs/radius.so
        doins pppd/plugins/radius/.libs/radattr.so
        doins pppd/plugins/radius/.libs/radrealms.so

        insinto /etc/ppp/radius
        insopts -m0644
        doins pppd/plugins/radius/etc/{dictionary*,issue,port-id-map,radiusclient.conf,realms,servers}

        doman pppd/plugins/radius/pppd-radius.8
        doman pppd/plugins/radius/pppd-radattr.8
    fi

    insinto /etc/modprobe.d
    insopts -m0644
    newins "${FILES}"/modules.ppp ppp.conf

    emagicdocs
    dodoc PLUGINS SETUP "${FILES}"/README.mpls
    edo cp -pPR {contrib,scripts} "${IMAGE}"/usr/share/doc/${PNVR}/

    dobin scripts/pon
    dobin scripts/poff
    dobin scripts/plog
    doman scripts/pon.1

    edo rmdir "${IMAGE}"/usr/share/doc/${PNVR}/contrib/pppgetpass/.libs
}

pkg_postinst() {
    echo
    elog "You should make sure you've enabled the following options (or a subset"
    elog "thereof) in your kernel. The options are listed in descending order of"
    elog "importance."
    echo
    elog "CONFIG_PPP            (REQUIRED)"
    if option activefilter ; then
        elog "CONFIG_PPP_FILTER     (REQUIRED by activefilter)"
    fi
    if option atm ; then
        elog "CONFIG_PPPOATM        (REQUIRED by pppoatm plugin)"
    fi
    elog "CONFIG_PPP_ASYNC      (Recommended)"
    elog "CONFIG_PPP_DEFLATE    (Recommended)"
    elog "CONFIG_PPP_BSDCOMP    (Recommended)"
    elog "CONFIG_PPPOE          (Optional, needed by pppoe plugin)"
    elog "CONFIG_PPP_SYNC_TTY   (Optional, used by the sync option)"
    elog "CONFIG_PPP_MPPE       (Optional, mostly used by PPTP links)"

    if [ ! -e "${ROOT}/dev/.udev" ] && [ ! -e "${ROOT}/dev/ppp" ]; then
        mknod "${ROOT}/dev/ppp" c 108 0
    fi

    # lib name has changed
    sed -i -e "s:^rp-pppoe.so:pppoe.so:" "${ROOT}/etc/ppp/options" ||
        eerror "sed /etc/ppp/options failed"

    echo
    elog "Pon, poff and plog scripts have been supplied for experienced users."
    elog "Users needing particular scripts (ssh,rsh,etc.) should check out the"
    elog "/usr/share/doc/${PNVR}/scripts directory."
}

