# Copyright 2008 Santiago M. Mola
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'libxslt-1.1.23.ebuild', which is:
#   Copyright 1999-2008 Gentoo Foundation

require gnome.org [ suffix=tar.xz ]
require python [ with_opt=true blacklist=none multibuild=false ]

SUMMARY="XSLT libraries and tools"
HOMEPAGE="https://gitlab.gnome.org/GNOME/${PN}/-/wikis/home"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="crypt debug doc examples"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libxml2:2.0[>=2.6.27]
        crypt? ( dev-libs/libgcrypt[>=1.1.92] )
        python? ( dev-python/libxml2[python_abis:*(-)?] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/bf59c338121b8b45d66ba6ecea69ad498015c396.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    # Always pass --with-debugger. It is required by third parties (see
    # e.g. Gentoo bug #98345)
    --with-debugger
    --with-plugins
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "crypt crypto"
    "debug"
    "python"
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( FEATURES )

src_prepare() {
    default

    # Fails for unknown reasons with musl
    [[ $(exhost --target) == *-musl* ]] && edo rm tests/REC/test-10-3.xsl

    # upstream: libxslt tests have always required the most recent libxml2 release, so this is expected
    # https://gitlab.gnome.org/GNOME/libxslt/-/issues/126
    ! has_version "dev-libs/libxml2:2.0[>=2.13]" && expatch "${FILES}"/${PN}-1.1.42-test.patch
}

src_configure() {
    # Use the target's libgcrypt-config. This is safe because libgcrypt-config is a standalone shell
    # script with /bin/sh as shebang.
    LIBGCRYPT_CONFIG=/usr/$(exhost --target)/bin/libgcrypt-config \
        default
}

src_install() {
    default

    keepdir /usr/$(exhost --target)/lib/${PN}-plugins

    if ! option doc; then
        edo rm -r "${IMAGE}"/usr/share/gtk-doc
    fi

    if ! option examples; then
        edo rm -rf "${IMAGE}"/usr/share/doc/${PNVR}/{python,tutorial{,2}}
    fi
}

