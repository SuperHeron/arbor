# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Copyright 2019 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require llvm-project
require lua [ whitelist="5.3" with_opt=true multibuild=false ]

SUMMARY="A next generation, high-performance debugger"

MYOPTIONS="
    doc
    libedit
    python
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? (
            app-doc/doxygen
            dev-python/epydoc[python_abis:*(-)?]
            media-gfx/graphviz
        )
    build+run:
        app-arch/xz
        dev-lang/clang:${LLVM_SLOT}[~${PV}]
        dev-lang/llvm:${LLVM_SLOT}[~${PV}]
        dev-libs/libxml2:2.0
        sys-libs/ncurses
        sys-libs/zlib
        libedit? ( dev-libs/libedit )
        lua? (
             dev-lang/swig[>=3.0]
         )
        python? ( dev-lang/swig[>=3.0] )
"

if ever at_least 16 ; then
    :
else
    DEPENDENCIES+="
        build+run:
        dev-python/six[python_abis:*(-)?]
    "
fi

# Fails a lot of tests, upstream is aware of it being broken
# http://lists.llvm.org/pipermail/llvm-dev/2019-September/135114.html
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DClang_DIR:STRING="${LLVM_PREFIX}"/lib/cmake/clang

    -DLLVM_ENABLE_ZLIB:BOOL=ON

    -DLLDB_ENABLE_CURSES:BOOL=ON
    -DLLDB_ENABLE_LZMA:BOOL=ON
    -DLLDB_USE_SYSTEM_SIX:BOOL=ON
)

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'doc LLDB_BUILD_DOCUMENTATION'
    'libedit LLDB_ENABLE_LIBEDIT'
    'lua LLDB_ENABLE_LUA'
    'python LLDB_ENABLE_PYTHON'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DLLDB_INCLUDE_TESTS:BOOL=TRUE -DLLDB_INCLUDE_TESTS:BOOL=FALSE'
)
