Title: libxcrypt 4.4.28 update
Author: Timo Gurr <tgurr@exherbo.org>
Content-Type: text/plain
Posted: 2023-03-12
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-libs/libxcrypt[<4.4.28]

libxcrypt is now slotted and SLOTs >= 0 remove the obsolete glibc
compatibility APIs. As a result the libcrypt.so is binary incompatible
with the one in SLOT=0, so make absolutely sure to rebuild at least
important system packages *before* attempting to forcefully remove
libxcrypt SLOT=0.

Bare minimum:
# cave resolve sys-libs/pam sys-apps/shadow -x1

Reinstall all dependents:
# cave resolve --reinstall-dependents-of libxcrypt:0 nothing

Failure to do so *will* result in a broken system.
